! Read Verbs <  lexicon

read lexc  < hindi-verbs.lexc;
define Verbs
!push defined Verbs

! Read IrregularVerbs lexicon
read lexc <  hindi-irverbs.lexc;
define IrVerbs;
!push defined IrVerbs

!Solve overgeneration by using priority union
read regex  (IrVerbs .P. Verbs);

